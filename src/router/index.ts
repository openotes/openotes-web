import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

import admin from './admin'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/admin',
    name: 'Admin dashboard',
    component: () => import(/* webpackChunkName: "admin" */ '@/views/AdminDashboard.vue'),
    children: admin,
  },
  {
    path: '/',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
