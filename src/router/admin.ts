import { RouteConfig } from 'vue-router'

const routes: Array<RouteConfig> = [
  { path: '/admin/schedule' },
  {
    path: '/admin/students',
    component: () =>
      import(/* webpackChunkName: "admin" */ '@/views/admin-dashboard/AdminDashboardStudents.vue'),
  },
  { path: '/admin/teachers' },
  { path: '/admin/admins' },
  { path: '/admin/lessons' },
  { path: '/admin/classes' },
  { path: '/admin/groups' },
  { path: '/admin/subjects' },
  { path: '/admin/classrooms' },
]

export default routes
