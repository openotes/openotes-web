import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

import en from 'vuetify/src/locale/en'
import fr from 'vuetify/src/locale/fr'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#9c27b0',
        secondary: '#2196f3',
        accent: '#ff9800',
        error: '#f44336',
        warning: '#ff5722',
        info: '#3f51b5',
        success: '#4caf50',
      },
    },
  },
  lang: {
    locales: { en, fr },
    current: navigator.language,
  },
})
