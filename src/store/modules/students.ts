import { Module, VuexModule, MutationAction } from 'vuex-module-decorators'
import api from '@/api'

@Module({ namespaced: true })
export default class Students extends VuexModule {
  all = {}
  current = {}

  @MutationAction({ mutate: ['all'] })
  async fetchAll(
    payload = { page: 1, limit: 15, search: '', orderBy: '', orderDesc: false }
  ): Promise<{ all: unknown }> {
    const response = await api.get('/students', {
      params: {
        page: payload.page,
        limit: payload.limit,
        search: payload.search,
        order_by: payload.orderBy,
        order_desc: payload.orderDesc,
      },
    })
    return { all: response.data }
  }

  get loading(): boolean {
    return Object.keys(this.all).length == 0
  }
}
