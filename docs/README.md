# Views

## `/admin` - Admin dashboard:

- `/schedule`: Search for student and get schedule
- `/students`: CRUD students
- `/teachers`: CRUD teachers
- `/admins`: CRUD admins
- `/lessons`: CRUD lessons
- `/classes`: CRUD classes
- `/groups`: CRUD groups
- `/subjects`: CRUD subjects
- `/classrooms`: CRUD classrooms

(CRUD: View, add, modify and delete)

### Ideas:

- A component to search for free classrooms on a time slot, can be used on multiple views (lessons creation, etc...)

## `/teacher` - Teacher dashboard:

## `/student` - Student dashboard:
